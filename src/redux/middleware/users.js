import {setLoader} from "../actions/ui";
import {setNotification} from "../actions/notification";
import {USERS, LOGIN_USERS, setUser, LOGGED_USER} from "../actions/user";
import {API_FAILURE, API_SUCCESS, API_URL, apiRequest} from "../actions/api";
import {setToken} from "../actions/header";

const OAUTH_URL  = API_URL + '/oauth/token';
const USERS_URL  = API_URL + '/api/users';
const MYDATA_URL = USERS_URL + '/me';

export const usersMiddleware = () => (next) => (action) => {
    next(action)

    switch (action.type) {
        case `${LOGIN_USERS} ${API_SUCCESS}`:
            next(setToken(action.payload))
            next(apiRequest({
                body: null,
                method: 'GET',
                url: MYDATA_URL,
                feature: LOGGED_USER,
            }))
            break;

        case `${LOGIN_USERS} ${API_FAILURE}`:
            console.log("action: ", action)
            next(setNotification(
                action.payload.error,
                action.type
            ));
            next(setLoader({
                state: {loading: false},
                feature: action.type
            }));
            break;

        case `${LOGGED_USER} ${API_SUCCESS}`:
            next(setUser({
                user: action.payload
            }));
            next(setLoader({
                state: {loading: false},
                feature: USERS
            }));
            break;

        case LOGIN_USERS:
            next( apiRequest({
                body: JSON.stringify(action.payload),
                method: 'POST',
                url: OAUTH_URL,
                feature: LOGIN_USERS,
            }))
            next(setLoader({
                state: {
                    loading: true,
                    message: "seu login aguarde"
                },
                feature: USERS
            }))
            break

        default:
            break
    }
}