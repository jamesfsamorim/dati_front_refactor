import {API_FAILURE, API_REQUEST, API_SUCCESS, apiError, apiFailure, apiSuccess} from "../actions/api";

export const apiMiddleware = ({dispatch, getState}) => (next) => (action) => {
    next(action);

    if (action.type.includes(API_REQUEST)) {
        const { url, method, feature } = action.meta;
        const body = action.payload
        const headers = getState().headers

        fetch(url, {body, method, headers})
            .then(response => response.json())
            .then(response => {
                if(typeof response.error !== "undefined") {
                    dispatch(apiFailure({response, feature}))
                }
                else {
                    dispatch(apiSuccess({response, feature}))
                }
            })
            .catch(error => dispatch(apiError({error, feature})))
    }
};
