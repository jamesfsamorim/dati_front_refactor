import { SET_LOADER, SET_LANGUAGE } from "../actions/ui";
import loadLanguage from "./../../languages/i18n"
const language = loadLanguage()

const initState = {
    loading: false,
    message: "",
    i18n: language,
};

export const uiReducer = (ui = initState, action) => {
    switch (true) {

        case action.type.includes(SET_LOADER):
            return {
                ...ui,
                loading: action.payload.loading,
                message: action.payload.message
            };

        case action.type.includes(SET_LANGUAGE):
            return {...ui, i18n: action.payload};

        default:
            return ui;
    }
};
