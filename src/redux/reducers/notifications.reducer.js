import {SET_NOTIFICATION} from "../actions/notification";

const initState = {
    message: ""
};

export const notificationsReducer = (notifications = initState, action) => {
    switch (true) {

        case action.type.includes(SET_NOTIFICATION):
            return {
                ...notifications,
                message: action.payload
            };

        default:
            return notifications;
    }
};
