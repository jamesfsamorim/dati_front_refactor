import {SET_TOKEN} from "../actions/header";

const initState = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

export const headersReducer = (headers = initState, action) => {
    switch (true) {

        case action.type.includes(SET_TOKEN):
            const payload = action.payload
            const type = payload.token_type
            const token = payload.access_token

            return {
                ...headers,
                'Authorization': type + ' ' + token
            };

        default:
            return headers;
    }
};
