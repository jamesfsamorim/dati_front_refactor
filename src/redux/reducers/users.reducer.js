import {SET_USERS} from "../actions/user";
const userLogged = JSON.parse(localStorage.getItem('user'))
const initState = userLogged ? userLogged.data : {};

export const usersReducer = (user = initState, action) => {
    switch (action.type) {

        case SET_USERS:
            return action.payload;

        default:
            return user;
    }
};
