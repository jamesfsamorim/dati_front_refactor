// action types
export const API_REQUEST = 'API_REQUEST';
export const API_SUCCESS = 'API_SUCCESS';
export const API_FAILURE = 'API_FAILURE';
export const API_ERROR   = 'API_ERROR';
export const API_URL     = 'http://' + window.location.hostname + ':8080'

// action creators
export const apiRequest = ({body, method, url, feature}) => ({
    type: `${feature} ${API_REQUEST}`,
    payload: body,
    meta: {method, url, feature}
});

export const apiSuccess = ({response, feature}) => ({
    type: `${feature} ${API_SUCCESS}`,
    payload: response,
    meta: {feature}
});

export const apiFailure = ({response, feature}) => ({
    type: `${feature} ${API_FAILURE}`,
    payload: response,
    meta: {feature}
});

export const apiError = ({error, feature}) => ({
    type: `${feature} ${API_ERROR}`,
    payload: error,
    meta: {feature}
});

// header request
export const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}