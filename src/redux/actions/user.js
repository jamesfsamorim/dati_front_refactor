// feature name
export const USERS = '[Users]';

// action types
export const FETCH_USER     = `${USERS} FETCH`;
export const SET_USERS      = `${USERS} SET`;
export const LOGIN_USERS    = `${USERS} LOGIN`;
export const LOGGED_USER    = `${USERS} LOGGED`;
export const LOGOUT_USERS   = `${USERS} LOGOUT`;

// action creators
export const fetchUsers = ({query}) => ({
    type: FETCH_USER,
    payload: query
});

export const setUser = ({user}) => ({
    type: SET_USERS,
    payload: user
});

export const loginUsers = (data) => ({
    type: LOGIN_USERS,
    payload: loginData(data)
});

export const logoutUsers = ({query}) => ({
    type: LOGOUT_USERS,
    payload: query
});

export const loginData = (data) => {
    data.client_id     = process.env.REACT_APP_CLIENT_ID
    data.client_secret = process.env.REACT_APP_CLIENT_KEY
    data.grant_type    = process.env.REACT_APP_GRANT_TYPE
    return data
}
