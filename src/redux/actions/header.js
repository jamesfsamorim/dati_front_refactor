// action types
export const SET_TOKEN   = 'SET_TOKEN'

// action creators
export const setToken = (payload) => ({
    type: SET_TOKEN,
    payload: payload,
});
