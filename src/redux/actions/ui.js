// action types
export const SET_LOADER   = 'SET_LOADER';
export const SET_LANGUAGE = 'SET_LANGUAGE';

// action creators
export const setLoader = ({state, feature}) => ({
    type: `${feature} ${SET_LOADER}`,
    payload: state,
    meta: {feature}
});

export const setLanguage = ({text, idiom}) => ({
    type: `${SET_LANGUAGE} ${idiom}`,
    payload: text,
});
