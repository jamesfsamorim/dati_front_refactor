import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom"
import { connect } from 'react-redux';
import Login from "./login";

class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {ui, user} = this.props
        const text = ui.i18n.text
        console.log("user: ", user)

        return (
            <Router>
                <div className="full-height">
                    {Object.keys(user).length !== 0 ?
                        <div>
                            Seja bem vindo {user.name}
                        </div>
                        :
                        <Login />
                    }
                </div>
            </Router>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    ui: state.ui,
    notifications: state.notifications
})

export default connect(mapStateToProps, {})(App);
