import React, { Component } from 'react';
import PropTypes from "prop-types"
import {Row, Col, Form, Alert} from 'antd'
import { connect } from 'react-redux';
import { loginUsers } from "../../redux/actions/user";
import img_login from "../../images/login.svg"
import logo_white from "../../images/logo-white.png"
import FormDati from "../../components/form/Form";
import Loader from "../../components/loader/Loader";

class Login extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {ui, login, notifications} = this.props
        const text = ui.i18n.text
        const Login = Form.create({name: "login_dati"})(FormDati)
        //Ideia de colocar isso em um json e importar junto com o middleware renderizando na pagina que precisar
        const fields = [
            {
                name: 'username', type: 'input',
                rules: {
                    rules: [
                        {required: true, message:"Insira o seu usuário"}
                    ]
                },
                attribute: {type: 'text', placeholder: "E-mail" }
            },
            {
                name: 'password', type: 'input',
                rules: {
                    rules: [
                        {required: true, message:"Insira a sua senha"}
                    ]
                },
                attribute: {type: 'password', placeholder: "Senha"}
            }
        ]

        return (
            <div className="full-height">
                <Loader loading={ui.loading} message={ui.message} />
                <Row className="full-height">

                    <Col className="full-height" xs={0} sm={0} md={8} lg={8} xl={8}>
                        <div className="bg-login-white">
                            <div className="login-content">
                                <h6 className="login-info-header">
                                    {text.login.title} <br/>
                                    {text.login.subtitle}
                                </h6><br/>
                                <p className="login-info">{text.login.description}</p><br/>
                                <div className="img-login-frame">
                                    <img src={img_login} alt="image_login"/>
                                </div>
                            </div>
                        </div>
                    </Col>

                    <Col className="full-height" xs={24} sm={24} md={16} lg={16} xl={16}>
                        <Col className="bg-login-blue" >
                            <Col className="login-form" xs={17} sm={17} md={12} lg={10} xl={10}>
                                <div className="container-login">
                                    <div>
                                        <img className="logo-frame" src={logo_white} alt="logo_dati"/><br/>
                                        <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                                            <span className="login-title">{text.login.login_title}</span>
                                        </Col>
                                    </div>
                                    <div>
                                        <Login layout="vertical" btnText={text.common.log_in} fields={fields} submit={login} />
                                        {notifications.message !== "" &&
                                        <Alert message={text.login.error[notifications.message]} type="error" showIcon/>
                                        }
                                    </div>
                                    <span className="login-copyright">{text.login.copyright}</span>
                                </div>
                            </Col>
                        </Col>
                    </Col>

                </Row>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ui: state.ui,
    notifications: state.notifications
})

const mapActionsToProps = {
    login: loginUsers
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    notifications: PropTypes.object.isRequired,
    ui: PropTypes.object.isRequired
}

export default connect(mapStateToProps, mapActionsToProps)(Login);