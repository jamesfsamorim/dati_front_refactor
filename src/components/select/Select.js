import React from 'react'
import { Select } from 'antd';

class SelectDati extends React.Component {
    render() {
        return (
            <Select {...this.props} />
        )
    }
}

export default SelectDati