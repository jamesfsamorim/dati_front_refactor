import React from 'react'

class Loader extends React.Component {
    render() {
        const { loading, message } = this.props
        if(loading) {
            return (
                <div className="modal-screen">
                    <div className="card-loader">
                        <div className="bar_loader">
                            <div className="ship"></div>
                            <p className="p_loader">Carregando {message}...</p>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            null
        )
    }
}

export default Loader