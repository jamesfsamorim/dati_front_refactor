import React from "react"
import { Form } from 'antd';
import InputDati from "../input/Input";
import SelectDati from "../select/Select";

class FormDati extends React.Component {
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }

    handleSubmit = (event) => {
        event.preventDefault()
        const {form, submit} = this.props
        form.validateFields((err, values) => {
            if (!err) {
                console.log('values: ', values);
                submit(values)
            }
        });
    };

    render() {
        const { layout, fields, btnText } = this.props
        const { getFieldDecorator, getFieldError, isFieldTouched } = this.props.form;

        // Only show error after a field is touched.
        return (
            <Form layout={layout} onSubmit={this.handleSubmit}>
                {fields.map( field => {
                    switch (field.type) {
                        case 'input':
                            const requiredError = isFieldTouched(field.name) && getFieldError(field.name)
                            const validate_status = field.rules.rules[0].required  ? requiredError : ""
                            return (
                                <Form.Item validateStatus={validate_status ? "error" : ""}
                                           help={validate_status || ""}
                                           key={field.name} hasFeedback>
                                    {getFieldDecorator(field.name, field.rules)(
                                    <InputDati {...field.attribute} />)}
                                </Form.Item>
                            )
                        case 'select':
                            return (
                                <Form.Item key={field.name}>
                                    <SelectDati {...field.attribute} />
                                </Form.Item>
                            )
                        default:
                            console.log("Valor do type passado errado, segue objeto passado: ", field)
                            return (
                                null
                            )
                    }
                })}
                <Form.Item>
                    <button className="btn btn-light btn-block" type="submit">
                        {btnText}
                    </button>
                </Form.Item>
            </Form>
        );
    }
}

export default FormDati