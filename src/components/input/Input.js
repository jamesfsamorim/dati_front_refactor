import React from 'react'
import { Input } from 'antd';

class InputDati extends React.Component {
    render() {
        return (
            <Input {...this.props} />
        )
    }
}

export default InputDati