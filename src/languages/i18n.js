import en from "./en.json";
import pt_br from "./pt_br.json";

const languages = {
    en,
    pt_br
};

export default function (idiom = "pt_br") {
    return {
        text: languages[idiom],
        idiom: idiom
    }
};